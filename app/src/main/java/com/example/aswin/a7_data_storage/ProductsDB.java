package com.example.aswin.a7_data_storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ProductsDB {
    private DBHelper dbHelper;

    public ProductsDB(Context ctx) {
        dbHelper = new DBHelper(ctx);
    }

    public int insertProduct(Product product) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(DBHelper.FIELD_PRODUCT_NAME, product.name);
        cv.put(DBHelper.FIELD_PRODUCT_QUANTITY, product.quantity);

        int id = (int) db.insert(DBHelper.TABLE_PRODUCTS, null, cv);

        db.close();

        return id;
    }

    public void updateProduct(Product product) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(DBHelper.FIELD_PRODUCT_NAME, product.name);
        cv.put(DBHelper.FIELD_PRODUCT_QUANTITY, product.quantity);

        db.update(DBHelper.TABLE_PRODUCTS, cv, DBHelper.FIELD_PRODUCTS_ID + " = ?", new String[]{"" + product.ID});

        db.close();
    }

    public Product getProduct(int ID) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String selection = DBHelper.FIELD_PRODUCTS_ID + " = ?";
        String[] selectionArgs = {"" + ID};
        Cursor cursor = db.query(DBHelper.TABLE_PRODUCTS, null,
                selection, selectionArgs, null, null, null);

        //for more than one rows
//        while(cursor.moveToNext()) {
//            //read cursor
//        }

        //for one row data
        cursor.moveToFirst();

        Product product = new Product();
        product.ID = cursor.getInt(cursor.getColumnIndex(DBHelper.FIELD_PRODUCTS_ID));
        product.name = cursor.getString(cursor.getColumnIndex(DBHelper.FIELD_PRODUCT_NAME));
        product.quantity = cursor.getInt(cursor.getColumnIndex(DBHelper.FIELD_PRODUCT_QUANTITY));

        cursor.close();
        db.close();

        return product;
    }
}
