package com.example.aswin.a7_data_storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper{

    public static final int DB_VERSION = 1;

    public static final String DB_NAME = "ProductsDB";

    public static final String TABLE_PRODUCTS = "Products";
    public static final String FIELD_PRODUCTS_ID = "ID";
    public static final String FIELD_PRODUCT_NAME = "Name";
    public static final String FIELD_PRODUCT_QUANTITY = "Quantity";

    private static final String createSql = "CREATE TABLE IF NOT EXISTS " + TABLE_PRODUCTS + "(" +
            FIELD_PRODUCTS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            FIELD_PRODUCT_NAME + " TEXT," +
            FIELD_PRODUCT_QUANTITY + " INTEGER)";

    private static final String dropSql = "DROP TABLE IF EXISTS " + TABLE_PRODUCTS;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createSql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(dropSql);
        onCreate(db);
    }
}
