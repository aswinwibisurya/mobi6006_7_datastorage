package com.example.aswin.a7_data_storage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText txtID;
    EditText txtName;
    EditText txtQuantity;

    ProductsDB productsDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtID = findViewById(R.id.txtID);
        txtName = findViewById(R.id.txtName);
        txtQuantity = findViewById(R.id.txtQuantity);

        productsDB = new ProductsDB(this);
    }

    public void saveProduct(View view) {
        Product product = new Product();

        if(txtID.getText().toString().isEmpty()) {
            product.name = txtName.getText().toString();
            product.quantity = Integer.parseInt(txtQuantity.getText().toString());

            int ID = productsDB.insertProduct(product);
            txtID.setText("" + ID);
        } else {
            product.ID = Integer.parseInt(txtID.getText().toString());
            product.name = txtName.getText().toString();
            product.quantity = Integer.parseInt(txtQuantity.getText().toString());

            productsDB.updateProduct(product);
        }
    }

    public void loadProduct(View view) {
        int ID = Integer.parseInt(txtID.getText().toString());

        Product product = productsDB.getProduct(ID);

        txtName.setText(product.name);
        txtQuantity.setText("" + product.quantity);
    }
}
